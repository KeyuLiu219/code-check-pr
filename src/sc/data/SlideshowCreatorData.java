package sc.data;

import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import javafx.collections.FXCollections;
import sc.SlideshowCreatorApp;

/**
 * This is the data component for SlideshowCreatorApp. It has all the data needed
 * to be set by the user via the User Interface and file I/O can set and get
 * all the data from this object
 * 
 * @author Richard McKenna & Keyu LIu
 */
public class SlideshowCreatorData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    SlideshowCreatorApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<Slide> slides;
     ObservableList<Slide> slide1;
     ObservableList<Slide> slide2;
     ObservableList<Slide> slide3;
     ObservableList<Slide> slide4;
      ObservableList<Slide> slide5;

    /**
     * This constructor will setup the required data structures for use.
     * 
     * @param initApp The application this data manager belongs to. 
     */
    public SlideshowCreatorData(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
        
        // MAKE THE SLIDES MODEL
        slides = FXCollections.observableArrayList();
        slide1 = FXCollections.observableArrayList();
        slide2 = FXCollections.observableArrayList();
        slide3 = FXCollections.observableArrayList();
        slide4 = FXCollections.observableArrayList();
        slide5 = FXCollections.observableArrayList();
    }
    
    // ACCESSOR METHOD
    public ObservableList<Slide> getSlides() {
        return slides;
    }
    
    /**
     * Called each time new work is created or loaded, it resets all data
     * and data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {

    }

    // FOR ADDING A SLIDE WHEN THERE ISN'T A CUSTOM SIZE
    public void addSlide(String fileName, String path, String caption, int originalWidth, int originalHeight) {
        Slide slideToAdd = new Slide(fileName, path, caption, originalWidth, originalHeight);
        slides.add(slideToAdd);
    }

    // FOR ADDING A SLIDE WITH A CUSTOM SIZE
    public void addSlide(String fileName, String path, String caption, int originalWidth, int originalHeight, int currentWidth, int currentHeight) {
        Slide slideToAdd = new Slide(fileName, path, caption, originalWidth, originalHeight);
        slideToAdd.setCurrentWidth(currentWidth);
        slideToAdd.setCurrentHeight(currentHeight);
        slides.add(slideToAdd);
    }
    
    public void addNonDuplicateSlide(String fileName, String path, String caption, int originalWidth, int originalHeight) {
        if (!hasSlide(fileName, path))
            addSlide(fileName, path, caption, originalWidth, originalHeight);        
    }
    
    public void addNonDuplicateSlide(String fileName, String path, String caption, int originalWidth, int originalHeight, int currentWidth, int currentHeight) {
        if (!hasSlide(fileName, path))
            addSlide(fileName, path, caption, originalWidth, originalHeight, currentWidth, currentHeight);        
    }
    
    public boolean hasSlide(String testFileName, String testPath) {
        for (Slide s : slides) {
            if (s.getFileName().equals(testFileName)
                    && s.getPath().equals(testPath))
                return true;
        }
        return false;
    }
    
    public void removeSlide(Slide slideToRemove) {
        slides.remove(slideToRemove);
    }
    
      public void addZip(String fileName, String path, String caption) {
        Slide slideToAdd = new Slide(fileName, path, caption);
        slides.add(slideToAdd);
    }

    /**
     * @return the slide1
     */
      
      
      
    public ObservableList<Slide> getSlide1() {
        return slide1;
    }

    /**
     * @return the slide2
     */
    public ObservableList<Slide> getSlide2() {
        return slide2;
    }

    /**
     * @return the slide3
     */
    public ObservableList<Slide> getSlide3() {
        return slide3;
    }

    /**
     * @return the slide4
     */
    public ObservableList<Slide> getSlide4() {
        return slide4;
    }

    /**
     * @return the slide5
     */
    public ObservableList<Slide> getSlide5() {
        return slide5;
    }
}