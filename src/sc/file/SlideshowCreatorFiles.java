package sc.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import sc.SlideshowCreatorApp;
import sc.data.SlideshowCreatorData;
import sc.data.Slide;
import static sc.workspace.SlideshowCreatorController.copy;
import sc.workspace.SlideshowCreatorWorkspace;

/**
 * This class serves as the file component for the Slideshow Creator App. It
 * provides all saving and loading services for the application.
 *
 * @author Richard McKenna & Keyu LIu
 */
public class SlideshowCreatorFiles implements AppFileComponent {

    // THIS IS THE APP ITSELF
    SlideshowCreatorApp app;

    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_SLIDES = "slides";
    static final String JSON_SLIDE = "slide";
    static final String JSON_FILE_NAME = "file_name";
    static final String JSON_PATH = "path";
    static final String JSON_CAPTION = "caption";
    static final String JSON_ORIGINAL_WIDTH = "original_width";
    static final String JSON_ORIGINAL_HEIGHT = "original_height";
    static final String JSON_CURRENT_WIDTH = "current_width";
    static final String JSON_CURRENT_HEIGHT = "current_height";

    public SlideshowCreatorFiles(SlideshowCreatorApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);

        // CLEAR THE OLD DATA OUT
        SlideshowCreatorData dataManager = (SlideshowCreatorData) data;
        dataManager.resetData();

        // NOW LOAD ALL THE DATA FROM THE json OBJECT
        JsonArray jsonSlidesArray = json.getJsonArray(JSON_SLIDES);
        for (int i = 0; i < jsonSlidesArray.size(); i++) {
            JsonObject jsonSlide = jsonSlidesArray.getJsonObject(i);
            String fileName = jsonSlide.getString(JSON_FILE_NAME);
            String path = jsonSlide.getString(JSON_PATH);
            String caption = jsonSlide.getString(JSON_CAPTION);
            int originalWidth = jsonSlide.getInt(JSON_ORIGINAL_WIDTH);
            int originalHeight = jsonSlide.getInt(JSON_ORIGINAL_HEIGHT);
            int currentWidth = jsonSlide.getInt(JSON_CURRENT_WIDTH);
            int currentHeight = jsonSlide.getInt(JSON_CURRENT_HEIGHT);
            dataManager.addSlide(fileName, path, caption, originalWidth, originalHeight, currentWidth, currentHeight);
        }
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        SlideshowCreatorData dataManager = (SlideshowCreatorData) data;

        // NOW BUILD THE SLIDES JSON OBJECTS TO SAVE
        JsonArrayBuilder slidesArrayBuilder = Json.createArrayBuilder();
        ObservableList<Slide> slides = dataManager.getSlides();
        for (Slide slide : slides) {
            JsonObject slideJson = Json.createObjectBuilder()
                    .add(JSON_FILE_NAME, slide.getFileName())
                    .add(JSON_PATH, slide.getPath())
                    .add(JSON_CAPTION, slide.getCaption())
                    .add(JSON_ORIGINAL_WIDTH, slide.getOriginalWidth())
                    .add(JSON_ORIGINAL_HEIGHT, slide.getOriginalHeight())
                    .add(JSON_CURRENT_WIDTH, slide.getCurrentWidth())
                    .add(JSON_CURRENT_HEIGHT, slide.getCurrentHeight()).build();
            slidesArrayBuilder.add(slideJson);
        }
        JsonArray slidesArray = slidesArrayBuilder.build();

        // THEN PUT IT ALL TOGETHER IN A JsonObject
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_SLIDES, slidesArray)
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        StringWriter sw = new StringWriter();
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }

    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void loadZip(AppDataComponent data, String filePath, File file) throws IOException {
        // ASK THE USER TO SELECT A DIRECTORY
        SlideshowCreatorData dataManager = (SlideshowCreatorData) data;
        dataManager.resetData();
//        FileChooser imageChooser = new FileChooser();
        //    PropertiesManager props = PropertiesManager.getPropertiesManager();
        //     imageChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
        //     File imageFile = imageChooser.showOpenDialog(app.getGUI().getWindow());
        //  ObservableList<Slide> slides = dataManager.getSlides();
        if (file != null) {
            String fileName = file.getName();
            String topathname = "./work/" + app.getGUI().getAppTitle() + "/blackboard/";
            File toFile = new File(topathname);
            if (fileName.toLowerCase().endsWith(".zip")) {
                String path = file.getPath();
                String caption = "";
                try {
                    copy(file, toFile);
                } catch (Exception ex) {
                    Logger.getLogger(SlideshowCreatorFiles.class.getName()).log(Level.SEVERE, null, ex);
                }

                dataManager.addZip(fileName, path, caption);
                //   Image slideShowImage = loadImage(path);
                //  int originalWidth = (int) slideShowImage.getWidth();
                //  int originalHeight = (int) slideShowImage.getHeight();
                // app.getFileComponent().

            }
        }
    }

    @Override
    public void loadRecent(AppDataComponent data, File file) throws IOException {
       SlideshowCreatorData dataManager = (SlideshowCreatorData) data;
       // dataManager.resetData();
        SlideshowCreatorWorkspace work = (SlideshowCreatorWorkspace)app.getWorkspaceComponent();

 
        String path1=file.getPath()+"/blackboard/";
        File file1=new File(path1);
        File[] insideFile=file1.listFiles();
        String textFiled1="Successfully extract files:\n";
        String extractFailString1="\nextract Errors:\n";
        
        for(int i = 0; i < insideFile.length; i++){
            Slide sli = new Slide(insideFile[i].getName(), insideFile[i].getAbsolutePath(), "");
            dataManager.getSlides().add(sli);
            textFiled1 += "-" + insideFile[i].getName() + "\n";
        }
        work.getExtractIndicator().setProgress(100);
        work.getExtractBar().setProgress(100);
        work.getNextButton().setDisable(false);
        work.getCaptionTextField().setEditable(false);
            work.getCaptionTextField().setText(textFiled1 + extractFailString1);
        work.getSlidesTableView().refresh();
        
        String path2=file.getPath()+"/submissions/";
        File file2=new File(path2);
        File[] insideFile2=file2.listFiles();
        String textFiled2 = "Successfully renamed sunmissions:\n";
        String extractFailString2 = "\nRename Errors:\n-none";
        for(int i = 0; i < insideFile2.length; i++){
            if(insideFile2[i].getName().endsWith(".zip")){
            Slide sli = new Slide(insideFile2[i].getName(), insideFile2[i].getAbsolutePath(), "");
            dataManager.getSlide1().add(sli);
            textFiled2 += "-" + insideFile2[i].getName() + "\n";
            }
        }
        work.getRenameTextField().setEditable(false);
            work.getRenameTextField().setText(textFiled2 + extractFailString2);
        work.getRenameIndicator().setProgress(100);
        work.getRenameBar().setProgress(100);
        work.getSlidesTableView2().refresh();
        
        String path3=file.getPath()+"/submissions/";
        File file3=new File(path3);
        File[] insideFile3=file3.listFiles();
        String textFiled3="Successfully unzipped files:\n";
        String extractFailString3="\nUnzip Errors:\n";
        for(int i = 0; i < insideFile3.length; i++){
            if(insideFile3[i].getName().endsWith(".zip")){
            Slide sli = new Slide(insideFile3[i].getName(), insideFile3[i].getAbsolutePath(), "");
            dataManager.getSlide2().add(sli);
            textFiled3 += "-" + insideFile3[i].getName() + "\n";
            }
        }
        work.getUnzipIndicator().setProgress(100);
        work.getUnzipBar().setProgress(100);
        work.getUnzipTextField().setEditable(false);
            work.getUnzipTextField().setText(textFiled3 + extractFailString3);
        work.getSlidesTableView3().refresh();
        
        
        
        
        String textFiled4="Successfully extract code files:\n";
        String extractFailString4="\nExtract code Errors:\n";
        //step4
        String path4=file.getPath()+"/projects/";
        File file4=new File(path4);
        File[] insideFile4=file4.listFiles();
        
        for(int i = 0; i < insideFile4.length; i++){
            if(insideFile4[i].exists()){
            Slide sli = new Slide(insideFile4[i].getName(), insideFile4[i].getAbsolutePath(), "");
            dataManager.getSlide4().add(sli);
            textFiled4 += "-" + insideFile4[i].getName() + "\n";
            }
        }
        work.getCodeIndicator().setProgress(100);
        work.getCodeBar().setProgress(100);
        work.getCodeTextField().setEditable(false);
            work.getCodeTextField().setText(textFiled4 + extractFailString4);
        work.getSlidesTableView4().refresh();
        
        
        //step5
        String path5=file.getPath()+"/projects/";
        File file5=new File(path5);
        File[] insideFile5=file5.listFiles();
        String resultString = "Student Plaglarism Check results can be found at\n";
            resultString += "http://www.google.com";
            work.getResultTextField().setText(resultString);
            work.getResultTextField().setEditable(false);
        for(int i = 0; i < insideFile5.length; i++){
            if(insideFile5[i].exists()){
            Slide sli = new Slide(insideFile5[i].getName(), insideFile5[i].getAbsolutePath(), "");
            dataManager.getSlide5().add(sli);
            
            }
        }
        
        work.getFinalIndicator().setProgress(100);
        work.getFinalBar().setProgress(100);
        work.getSlidesTableView5().refresh();
        
        
//        
//        Slide slide2 = new Slide(file.getName() + ".zip", file.getAbsolutePath(), "");
//        dataManager.getSlide2().add(slide2);
//        Slide slide3 = new Slide(file.getName() + ".zip", file.getAbsolutePath(), "");
//        dataManager.getSlide3().add(slide3);
//        Slide slide4 = new Slide(file.getName() + ".zip", file.getAbsolutePath(), "");
//        dataManager.getSlide4().add(slide4);
//        Slide slide5 = new Slide(file.getName() + ".zip", file.getAbsolutePath(), "");
//        dataManager.getSlide5().add(slide5);
    }
}
