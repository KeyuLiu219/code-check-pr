package sc.workspace;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import static djf.ui.AppGUI.CLASS_BORDERED_PANE;
import static djf.ui.AppGUI.CLASS_EDIT_BUTTON;
import static djf.ui.AppGUI.CLASS_NONBORDERED_PANE;
import static djf.ui.AppGUI.PROGRESS_BAR;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipException;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import sc.SlideshowCreatorApp;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.Duration;
import properties_manager.PropertiesManager;
import static sc.SlideshowCreatorProp.ADD_ALL_IMAGES_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.ADD_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.EXTR_PROGRESS_TEXT;
import static sc.SlideshowCreatorProp.FILE_NAME_COLUMN_TEXT;
import static sc.SlideshowCreatorProp.REFRESH_TABLE_TEXT;
import static sc.SlideshowCreatorProp.REMOVE_IMAGE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.REMOVE_TABLE_ITEM_TEXT;
import static sc.SlideshowCreatorProp.STEP_ONE_SENTENCE;
import static sc.SlideshowCreatorProp.STEP_ONE_TEXT;
import static sc.SlideshowCreatorProp.UPDATE_BUTTON_TEXT;
import static sc.SlideshowCreatorProp.VIEW_TABLE_ITEM_TEXT;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;
import static sc.style.SlideshowCreatorStyle.CLASS_EDIT_TEXT_FIELD;
import static sc.style.SlideshowCreatorStyle.CLASS_PROMPT_LABEL;
import static sc.style.SlideshowCreatorStyle.CLASS_SENTENCE_LABEL;
import static sc.style.SlideshowCreatorStyle.CLASS_SLIDES_TABLE;
import static sc.style.SlideshowCreatorStyle.CLASS_UPDATE_BUTTON;
import static sc.workspace.SlideshowCreatorController.copy;

/**
 * This class serves as the workspace component for the TA Manager application.
 * It provides all the user interface controls in the workspace area.
 *
 * @author Richard McKenna & Keyu LIu
 */
public class SlideshowCreatorWorkspace extends AppWorkspaceComponent {

    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    SlideshowCreatorApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    SlideshowCreatorController controller;
    CopyTask copyTask;
    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    HBox editToolbar;
    CopyTask1 copyTask1;
    CopyTask2 copyTask2;
    CopyTask3 copyTask3;
    CopyTask4 copyTask4;

    WebView browser;
    WebEngine webEngine;
    Button homeButton;
    private Button previousButton;
    private Button nextButton;

    Button removeButton;
    Button refreshButton;
    Button viewButton;

    Button removeButton2;
    Button refreshButton2;
    Button viewButton2;

    Button removeButton3;
    Button refreshButton3;
    Button viewButton3;

    Button removeButton4;
    Button refreshButton4;
    Button viewButton4;

    Button removeButton5;
    Button refreshButton5;
    Button viewButton5;

    private static int step = 1;

    private GridPane workspaceBorderPane;

    // content for filesTableScrollPane FOR THE SLIDES TABLE  
    private TableView<Slide> slidesTableView;
    TableColumn<Slide, StringProperty> fileNameColumn;
    // THE EDIT PANE
    private GridPane editPane;
    private GridPane extractPane;

    private GridPane editPane2;
    private GridPane extractPane2;

    private GridPane editPane3;
    private GridPane extractPane3;

    private GridPane editPane4;
    private GridPane extractPane4;

    private GridPane editPane5;
    private GridPane extractPane5;

    //panel for step 1
    Label step1;
    Label step1SentenceLabel;
    ScrollPane filesTableScrollPane;
    HBox multiplePanes;

    HBox multiplePanes2;
    HBox multiplePanes3;
    HBox multiplePanes4;
    HBox multiplePanes5;

    HBox step5Box;

    GridPane srcFileTypePane;
    Label srcTypeLabel;
    CheckBox javaBox;
    CheckBox jsBox;
    CheckBox chcppBox;
    CheckBox csBox;
    CheckBox other;
    TextField otherField;

    HBox bar1;
    HBox bar2;
    HBox bar3;
    HBox bar4;
    HBox bar5;

    //progressbar
    int numTasks = 0;
    ReentrantLock progressLock;

    Label etrPrgLabel;
    private Button extractButton;
    ScrollPane textPane;
    private TextArea captionTextField;//content of textPane   

    //panle for step2
    private Label step2;
    private Label stepSentenceLabe2;
    private Label renPrgLabel;
    private Button renameButton;
    TextArea renameTextField;
    private TableView<Slide> slidesTableView2;
    private TableColumn<Slide, StringProperty> fileNameColum2;
    ScrollPane filesTableScrollPane2;
    ScrollPane textPane2;

    private Label step3;
    private Label stepSentenceLabe3;
    private Label unzipPrgLabel;
    private Button unzipButton;
    TextArea unzipTextField;
    private TableView<Slide> slidesTableView3;
    private TableColumn<Slide, StringProperty> fileNameColum3;
    ScrollPane filesTableScrollPane3;
    ScrollPane textPane3;

    private Label step4;
    private Label stepSentenceLabe4;
    private Label codePrgLabel;
    private Button codeButton;
    TextArea codeTextField;
    private TableView<Slide> slidesTableView4;
    private TableColumn<Slide, StringProperty> fileNameColum4;
    ScrollPane filesTableScrollPane4;
    ScrollPane textPane4;

    private Label step5;
    private Label stepSentenceLabe5;
    private Label checkPrgLabel;
    private Button checkButton;
    private Button resultButton;
    TextArea resultTextField;
    private TableView<Slide> slidesTableView5;
    private TableColumn<Slide, StringProperty> fileNameColum5;
    private ScrollPane filesTableScrollPane5;
    private ScrollPane textPane5;

//    ProgressBar extractBar = new ProgressBar();
//    ProgressBar renameBar = new ProgressBar();
private ProgressBar extractBar = new ProgressBar();
    private ProgressIndicator extractIndicator = new ProgressIndicator();
    private ProgressBar renameBar = new ProgressBar();
    private ProgressIndicator renameIndicator = new ProgressIndicator();
    private ProgressBar unzipBar = new ProgressBar();
    private ProgressIndicator unzipIndicator = new ProgressIndicator();
    private ProgressBar codeBar = new ProgressBar();
    private ProgressIndicator codeIndicator = new ProgressIndicator();
    private ProgressBar finalBar = new ProgressBar();
    private ProgressIndicator finalIndicator = new ProgressIndicator();

    Timeline task = new Timeline(
            new KeyFrame(
                    Duration.ZERO,
                    new KeyValue(getExtractBar().progressProperty(), 0)
            ),
            new KeyFrame(
                    Duration.seconds(2),
                    new KeyValue(getExtractBar().progressProperty(), 1)
            )
    );

    /**
     * The constructor initializes the user interface for the workspace area of
     * the application.
     */
    public SlideshowCreatorWorkspace(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // LAYOUT THE APP
        initLayout();

        // HOOK UP THE CONTROLLERS
        initControllers();

        // AND INIT THE STYLE FOR THE WORKSPACE
        initStyle();
    }

    private void initLayout() {
        // WE'LL USE THIS TO GET UI TEXT
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // FIRST MAKE ALL THE COMPONENTS
        editToolbar = new HBox();
        multiplePanes = new HBox();
        multiplePanes2 = new HBox();
        multiplePanes3 = new HBox();
        multiplePanes4 = new HBox();
        multiplePanes5 = new HBox();

        step5Box = new HBox();
        srcFileTypePane = new GridPane();
        srcTypeLabel = new Label("Source File Types");
        javaBox = new CheckBox(".java");
        jsBox = new CheckBox(".js");
        chcppBox = new CheckBox(".c, .h, .cpp");
        csBox = new CheckBox(".cs");
        other = new CheckBox();
        otherField = new TextField();

        bar1 = new HBox();
        bar2 = new HBox();
        bar3 = new HBox();
        bar4 = new HBox();
        bar5 = new HBox();

        previousButton = new Button(props.getProperty(ADD_IMAGE_BUTTON_TEXT));
        homeButton = new Button(props.getProperty(ADD_ALL_IMAGES_BUTTON_TEXT));
        nextButton = new Button(props.getProperty(REMOVE_IMAGE_BUTTON_TEXT));

        removeButton = new Button(props.getProperty(REMOVE_TABLE_ITEM_TEXT));
        refreshButton = new Button(props.getProperty(REFRESH_TABLE_TEXT));
        viewButton = new Button(props.getProperty(VIEW_TABLE_ITEM_TEXT));

        removeButton2 = new Button(props.getProperty(REMOVE_TABLE_ITEM_TEXT));
        refreshButton2 = new Button(props.getProperty(REFRESH_TABLE_TEXT));
        viewButton2 = new Button(props.getProperty(VIEW_TABLE_ITEM_TEXT));

        removeButton3 = new Button(props.getProperty(REMOVE_TABLE_ITEM_TEXT));
        refreshButton3 = new Button(props.getProperty(REFRESH_TABLE_TEXT));
        viewButton3 = new Button(props.getProperty(VIEW_TABLE_ITEM_TEXT));

        removeButton4 = new Button(props.getProperty(REMOVE_TABLE_ITEM_TEXT));
        refreshButton4 = new Button(props.getProperty(REFRESH_TABLE_TEXT));
        viewButton4 = new Button(props.getProperty(VIEW_TABLE_ITEM_TEXT));

        removeButton5 = new Button(props.getProperty(REMOVE_TABLE_ITEM_TEXT));
        refreshButton5 = new Button(props.getProperty(REFRESH_TABLE_TEXT));
        viewButton5 = new Button(props.getProperty(VIEW_TABLE_ITEM_TEXT));

        filesTableScrollPane = new ScrollPane();
        textPane = new ScrollPane();

        filesTableScrollPane2 = new ScrollPane();
        textPane2 = new ScrollPane();
        filesTableScrollPane3 = new ScrollPane();
        textPane3 = new ScrollPane();
        filesTableScrollPane4 = new ScrollPane();
        textPane4 = new ScrollPane();
        setFilesTableScrollPane5(new ScrollPane());
        setTextPane5(new ScrollPane());

        slidesTableView = new TableView();
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));

        slidesTableView2 = new TableView();
        fileNameColum2 = new TableColumn("Student Submissions");
        slidesTableView3 = new TableView();
        fileNameColum3 = new TableColumn("Student ZIP Files");
        slidesTableView4 = new TableView();
        fileNameColum4 = new TableColumn("Student Work Directories");
        slidesTableView5 = new TableView();
        fileNameColum5 = new TableColumn("Student Work");

        editPane = new GridPane();
        extractPane = new GridPane();

        editPane2 = new GridPane();
        extractPane2 = new GridPane();

        editPane3 = new GridPane();
        extractPane3 = new GridPane();

        editPane4 = new GridPane();
        extractPane4 = new GridPane();

        editPane5 = new GridPane();
        extractPane5 = new GridPane();

        // THE EDIT BUTTONS START OUT DISABLED
        getPreviousButton().setDisable(true);
        homeButton.setDisable(true);
        getNextButton().setDisable(true);

        etrPrgLabel = new Label(props.getProperty(EXTR_PROGRESS_TEXT));
        captionTextField = new TextArea();
        step1 = new Label(props.getProperty(STEP_ONE_TEXT));
        step1SentenceLabel = new Label(props.getProperty(STEP_ONE_SENTENCE));
        setExtractButton(new Button(props.getProperty(UPDATE_BUTTON_TEXT)));

        //prgressbar
        setExtractBar(new ProgressBar(0));
        setExtractIndicator(new ProgressIndicator(0));

        setRenameBar(new ProgressBar(0));
        setRenameIndicator(new ProgressIndicator(0));
        setUnzipBar(new ProgressBar(0));
        setUnzipIndicator(new ProgressIndicator(0));
        setCodeBar(new ProgressBar(0));
        setCodeIndicator(new ProgressIndicator(0));
        setFinalBar(new ProgressBar(0));
        setFinalIndicator(new ProgressIndicator(0));

//rename
        renPrgLabel = new Label("Rename Progress");
        renameTextField = new TextArea();
        step2 = new Label("Step2: Rename Student Submissions");
        stepSentenceLabe2 = new Label("Click the Rename button to rename all submissions");
        renameButton = new Button("Rename");

        //unzip
        unzipPrgLabel = new Label("Unzip Progress");
        unzipTextField = new TextArea();
        step3 = new Label("Step3: Unzip Student Submissions");
        stepSentenceLabe3 = new Label("Select student sunmissions and click Unzip.");
        unzipButton = new Button("Unzip");

        //code extract
        codePrgLabel = new Label("Code Progress");
        codeTextField = new TextArea();
        step4 = new Label("Step4: Extract Source Code");
        stepSentenceLabe4 = new Label("Select student and click Extract Code");
        codeButton = new Button("Extract Code");

        //final
        checkPrgLabel = new Label("Check Progress");
        resultTextField = new TextArea();
        step5 = new Label("Step5: Rename Student Submissions");
        stepSentenceLabe5 = new Label("Click the Rename button to rename all submissions");
        checkButton = new Button("Code Check");
        resultButton = new Button("View Results");

        // ARRANGE THE TABLE
        fileNameColumn = new TableColumn(props.getProperty(FILE_NAME_COLUMN_TEXT));
        getSlidesTableView().getColumns().add(fileNameColumn);
        fileNameColumn.prefWidthProperty().bind(getSlidesTableView().widthProperty());
        fileNameColumn.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );

        fileNameColum2 = new TableColumn("Student Submissions");
        slidesTableView2.getColumns().add(fileNameColum2);
        fileNameColum2.prefWidthProperty().bind(slidesTableView2.widthProperty());
        fileNameColum2.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );

        fileNameColum3 = new TableColumn("Student ZIP Files");
        slidesTableView3.getColumns().add(fileNameColum3);
        fileNameColum3.prefWidthProperty().bind(slidesTableView3.widthProperty());
        fileNameColum3.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );

        fileNameColum4 = new TableColumn("Student Work Directories");
        slidesTableView4.getColumns().add(fileNameColum4);
        fileNameColum4.prefWidthProperty().bind(slidesTableView4.widthProperty());
        fileNameColum4.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );

        fileNameColum5 = new TableColumn("Student Work");
        slidesTableView5.getColumns().add(fileNameColum5);
        fileNameColum5.prefWidthProperty().bind(slidesTableView5.widthProperty());
        fileNameColum5.setCellValueFactory(
                new PropertyValueFactory<Slide, StringProperty>("fileName")
        );

        // HOOK UP THE TABLE TO THE DATA
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
        ObservableList<Slide> model = data.getSlides();
        getSlidesTableView().setItems(model);

        ObservableList<Slide> model1 = data.getSlide1();
        getSlidesTableView2().setItems(model1);

        ObservableList<Slide> model2 = data.getSlide2();
        getSlidesTableView3().setItems(model2);

        ObservableList<Slide> model4 = data.getSlide4();
        getSlidesTableView4().setItems(model4);

        ObservableList<Slide> model5 = data.getSlide5();
        getSlidesTableView5().setItems(model5);

        // THEM ORGANIZE THEM  
        // No need to change
        editToolbar.getChildren().add(homeButton);
        editToolbar.getChildren().add(getPreviousButton());
        editToolbar.getChildren().add(getNextButton());
        multiplePanes.getChildren().add(removeButton);
        multiplePanes.getChildren().add(refreshButton);
        multiplePanes.getChildren().add(viewButton);

        multiplePanes2.getChildren().add(removeButton2);
        multiplePanes2.getChildren().add(refreshButton2);
        multiplePanes2.getChildren().add(viewButton2);

        multiplePanes3.getChildren().add(removeButton3);
        multiplePanes3.getChildren().add(refreshButton3);
        multiplePanes3.getChildren().add(viewButton3);

        multiplePanes4.getChildren().add(removeButton4);
        multiplePanes4.getChildren().add(refreshButton4);
        multiplePanes4.getChildren().add(viewButton4);

        multiplePanes5.getChildren().add(removeButton5);
        multiplePanes5.getChildren().add(refreshButton5);
        multiplePanes5.getChildren().add(viewButton5);

        step5Box.getChildren().add(checkButton);
        step5Box.getChildren().add(resultButton);

        srcFileTypePane.add(srcTypeLabel, 0, 0);
        srcFileTypePane.add(javaBox, 0, 1);
        srcFileTypePane.add(jsBox, 1, 1);
        srcFileTypePane.add(chcppBox, 0, 2);
        srcFileTypePane.add(csBox, 1, 2);
        srcFileTypePane.add(other, 0, 3);
        srcFileTypePane.add(otherField, 1, 3);

//        bar1.getChildren().add(etrPrgLabel);
//        bar1.getChildren().add(extractBar);
//        bar1.getChildren().add(exPctLabel);
//        extractBar.setPrefWidth(250);


//     bar1.getChildren().add(etrPrgLabel);
//        bar1.getChildren().add(getExtractBar());
//        bar1.getChildren().add(getExtractIndicator());
//        
//        getExtractBar().setPrefWidth(250);
//        getExtractIndicator().setPrefWidth(100);
//
//        bar2.getChildren().add(renPrgLabel);
//        bar2.getChildren().add(getRenameBar());
//        bar2.getChildren().add(getRenameIndicator());
//        //bar2.getChildren().add(rePctLabel);
//        getRenameBar().setPrefWidth(250);
//        getRenameIndicator().setPrefWidth(100);
//
//        bar3.getChildren().add(unzipPrgLabel);
//        bar3.getChildren().add(getUnzipBar());
//        bar3.getChildren().add(getUnzipIndicator());
//        //bar3.getChildren().add(unPctLabel);
//        getUnzipBar().setPrefWidth(250);
//        getUnzipIndicator().setPrefWidth(100);
//
//        bar4.getChildren().add(codePrgLabel);
//        bar4.getChildren().add(getCodeBar());
//        bar4.getChildren().add(getCodeIndicator());
//        getCodeBar().setPrefWidth(250);
//        getCodeIndicator().setPrefWidth(100);
//        //bar4.getChildren().add(cdPctLabel);
//
//
//        bar5.getChildren().add(checkPrgLabel);
//        bar5.getChildren().add(getFinalBar());
//        bar5.getChildren().add(getFinalIndicator());
//        //bar5.getChildren().add(resultPctLabel);
//        getFinalBar().setPrefWidth(250);
//        getFinalIndicator().setPrefWidth(100);

        filesTableScrollPane.setContent(getSlidesTableView());

        textPane.setContent(getCaptionTextField());

        getExtractPane().add(step1, 0, 0);
        getExtractPane().add(step1SentenceLabel, 0, 1);
        getExtractPane().add(filesTableScrollPane, 0, 2);
        getExtractPane().add(multiplePanes, 0, 3);
        getEditPane().add(bar1, 0, 0);
        getEditPane().add(getExtractButton(), 0, 1);
        getEditPane().add(textPane, 0, 2);

        filesTableScrollPane2.setContent(slidesTableView2);
        textPane2.setContent(renameTextField);
        getExtractPane2().add(multiplePanes2, 0, 3);
        getExtractPane2().add(step2, 0, 0);
        getExtractPane2().add(stepSentenceLabe2, 0, 1);
        getEditPane2().add(getRenameButton(), 0, 1);
        editPane2.add(bar2, 0, 0);
        getEditPane2().add(textPane2, 0, 2);
        getExtractPane2().add(filesTableScrollPane2, 0, 2);

        getExtractPane3().add(multiplePanes3, 0, 3);
        getExtractPane3().add(getStep3(), 0, 0);
        getExtractPane3().add(getStepSentenceLabe3(), 0, 1);
        getExtractPane4().add(srcFileTypePane, 0, 4);
        getEditPane3().add(bar3, 0, 0);
        getEditPane3().add(getUnzipButton(), 0, 1);
        getEditPane3().add(textPane3, 0, 2);
        getExtractPane3().add(filesTableScrollPane3, 0, 2);
        filesTableScrollPane3.setContent(slidesTableView3);
        textPane3.setContent(unzipTextField);

        getExtractPane4().add(multiplePanes4, 0, 3);
        getExtractPane4().add(getStep4(), 0, 0);
        getExtractPane4().add(getStepSentenceLabe4(), 0, 1);
        getEditPane4().add(bar4, 0, 0);
        getEditPane4().add(getCodeButton(), 0, 1);
        getEditPane4().add(textPane4, 0, 2);
        getExtractPane4().add(filesTableScrollPane4, 0, 2);
        filesTableScrollPane4.setContent(slidesTableView4);
        textPane4.setContent(codeTextField);

        getExtractPane5().add(multiplePanes5, 0, 3);
        getExtractPane5().add(getStep5(), 0, 0);
        getExtractPane5().add(getStepSentenceLabe5(), 0, 1);
        getEditPane5().add(bar5, 0, 0);
        getEditPane5().add(step5Box, 0, 1);
//        getEditPane5().add(getResultButton(), 1, 1);
        getEditPane5().add(getTextPane5(), 0, 2);
        getExtractPane5().add(getFilesTableScrollPane5(), 0, 2);
        getFilesTableScrollPane5().setContent(slidesTableView5);
        getTextPane5().setContent(resultTextField);

        // DISABLE THE DISPLAY TEXT FIELDS
        getCaptionTextField().setDisable(true);
        getExtractButton().setDisable(true);

        // AND THEN PUT EVERYTHING INSIDE THE WORKSPACE
        app.getGUI().getTopToolbarPane().getChildren().add(editToolbar);

        workspaceBorderPane = new GridPane();

        filesTableScrollPane.setFitToWidth(true);
        filesTableScrollPane.setFitToHeight(true);

        filesTableScrollPane2.setFitToWidth(true);
        filesTableScrollPane2.setFitToHeight(true);
        filesTableScrollPane3.setFitToWidth(true);
        filesTableScrollPane3.setFitToHeight(true);
        filesTableScrollPane4.setFitToWidth(true);
        filesTableScrollPane4.setFitToHeight(true);
        getFilesTableScrollPane5().setFitToWidth(true);
        getFilesTableScrollPane5().setFitToHeight(true);

        getCaptionTextField().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        renameTextField.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        unzipTextField.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        codeTextField.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        resultTextField.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);

        getExtractPane().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        getEditPane().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);

        getExtractPane2().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        getEditPane2().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);;

        getExtractPane3().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        getEditPane3().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);;

        getExtractPane4().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        getEditPane4().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);;

        getExtractPane5().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        getEditPane5().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);;

        filesTableScrollPane.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        textPane.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        textPane.setFitToWidth(true);
        textPane.setFitToHeight(true);

        filesTableScrollPane2.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        textPane2.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        textPane2.setFitToWidth(true);
        textPane2.setFitToHeight(true);

        filesTableScrollPane3.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        textPane3.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        textPane3.setFitToWidth(true);
        textPane3.setFitToHeight(true);

        filesTableScrollPane4.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        textPane4.setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        textPane4.setFitToWidth(true);
        textPane4.setFitToHeight(true);

        getFilesTableScrollPane5().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        getTextPane5().setPrefSize(app.getGUI().getWindow().getHeight(), app.getGUI().getWindow().getWidth() / 2);
        getTextPane5().setFitToWidth(true);
        getTextPane5().setFitToHeight(true);

        getWorkspaceBorderPane().add(getExtractPane(), 0, 0);
        getWorkspaceBorderPane().add(getEditPane(), 1, 0);

//        workspaceBorderPane.setCenter(extractPane);
//        workspaceBorderPane.setRight(editPane);
        // AND SET THIS AS THE WORKSPACE PANE
        workspace = getWorkspaceBorderPane();
    }

    private void initControllers() {
        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new SlideshowCreatorController(app);

//        previousButton.setOnAction(e -> {
//            controller.handleAddAllImagesInDirectory();
//        });
        homeButton.setOnAction(e -> {
//            controller.homePage();
//            setStep(1);

            controller.handleStepScene(1);
            setStep(1);
        });

        getNextButton().setOnAction(e -> {
//            controller.handleNextScene(getStep());
//            setStep(getStep() + 1);
            controller.handleStepScene(getStep() + 1);
            setStep(getStep() + 1);
        });

        removeButton.setOnAction(e -> {

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("comfirmation window");
            alert.setHeaderText(null);
            alert.setContentText("Are you sure remove the file permanently?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
                Slide selectedSlide = workspace.getSlidesTableView().getSelectionModel().getSelectedItem();
                SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                data.getSlides().remove(selectedSlide);
                String pathString = "./work/" + app.getGUI().getAppTitle() + "/blackboard/" + selectedSlide.getFileName();
                System.out.println("Slide1 deletepath is" + selectedSlide.getPath());
                System.out.println("Slide1 name is " + selectedSlide.getFileName());
                File deleteFile = new File("./work/" + app.getGUI().getAppTitle() + "/blackboard/" + selectedSlide.getFileName());
                deleteFile.delete();
                removeButton2.setDisable(false);
                // ... user chose OK
            } else {
                // ... user chose CANCEL or closed the dialog

                e.consume();
            }

        });

        removeButton2.setOnAction(e -> {

            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("comfirmation window");
            alert.setHeaderText(null);
            alert.setContentText("Are you sure remove the file permanently?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
                Slide selectedSlide = workspace.getSlidesTableView2().getSelectionModel().getSelectedItem();
                SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                data.getSlide1().remove(selectedSlide);
                String pathString = "./work/" + app.getGUI().getAppTitle() + "/submissions/" + selectedSlide.getFileName();
                System.out.println("Slide2 deletepath is" + selectedSlide.getPath());
                System.out.println("Slide2 name is " + selectedSlide.getFileName());
                File deleteFile = new File(pathString);
                deleteFile.delete();
                removeButton3.setDisable(false);

            } else {
                // ... user chose CANCEL or closed the dialog

                e.consume();
            }

        });//remove ok

        removeButton3.setOnAction(e -> {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("comfirmation window");
            alert.setHeaderText(null);
            alert.setContentText("Are you sure remove the file permanently?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
                Slide selectedSlide = workspace.getSlidesTableView3().getSelectionModel().getSelectedItem();
                SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                data.getSlide2().remove(selectedSlide);
                String pathString = "./work/" + app.getGUI().getAppTitle() + "/submissions/" + selectedSlide.getFileName();
                System.out.println("Slide3 deletepath is" + selectedSlide.getPath());
                System.out.println("Slide3 name is " + selectedSlide.getFileName());

                File deleteFile = new File(pathString);
                deleteFile.delete();
                removeButton4.setDisable(false);
            } else {
                // ... user chose CANCEL or closed the dialog

                e.consume();
            }
        });

        removeButton4.setOnAction(e -> {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("comfirmation window");
            alert.setHeaderText(null);
            alert.setContentText("Are you sure remove the file permanently?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {

                SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
                Slide selectedSlide = workspace.getSlidesTableView4().getSelectionModel().getSelectedItem();
                SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                data.getSlide4().remove(selectedSlide);
                String pathString = "./work/" + app.getGUI().getAppTitle() + "/projects/" + selectedSlide.getFileName();
                System.out.println("Slide4 deletepath is" + selectedSlide.getPath());
                System.out.println("Slide4 name is " + selectedSlide.getFileName());

                File deleteFile = new File(pathString);
                if (deleteFile.isFile()) {//判断是否是文件  
                    deleteFile.delete();//删除文件   
                } else if (deleteFile.isDirectory()) {//否则如果它是一个目录  

                    File[] files = deleteFile.listFiles();//声明目录下所有的文件 files[];  
                    for (int i = 0; i < files.length; i++) {//遍历目录下所有的文件  
                        //this.deleteFile(files[i]);//把每个文件用这个方法进行迭代  
                        files[i].delete();
                    }
                    deleteFile.delete();//删除文件夹  
                }
                //deleteFile.delete();
                removeButton5.setDisable(false);
            } else {
                // ... user chose CANCEL or closed the dialog

                e.consume();
            }
        });

        removeButton5.setOnAction(e -> {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.setTitle("comfirmation window");
            alert.setHeaderText(null);
            alert.setContentText("Are you sure remove the file permanently?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {

                SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
                Slide selectedSlide = workspace.getSlidesTableView5().getSelectionModel().getSelectedItem();
                SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                data.getSlide5().remove(selectedSlide);

                System.out.println("Slide5 deletepath is" + selectedSlide.getPath());
                System.out.println("Slide5 name is " + selectedSlide.getFileName());
                String pathString = "./work/" + app.getGUI().getAppTitle() + "/code/" + selectedSlide.getFileName();
                File deleteFile = new File(pathString);
                if (deleteFile.isFile()) {//判断是否是文件  
                    deleteFile.delete();//删除文件   
                } else if (deleteFile.isDirectory()) {//否则如果它是一个目录  
                    File[] files = deleteFile.listFiles();//声明目录下所有的文件 files[];  
                    for (int i = 0; i < files.length; i++) {//遍历目录下所有的文件  
                        //this.deleteFile(files[i]);//把每个文件用这个方法进行迭代  
                        files[i].delete();
                    }
                    deleteFile.delete();//删除文件夹  
                }
            } else {
                // ... user chose CANCEL or closed the dialog

                e.consume();
            }
        });

        refreshButton.setOnAction(e -> {
            getSlidesTableView().refresh();
            System.out.println("step1 table is refreshed");
        });

        refreshButton2.setOnAction(e -> {
            getSlidesTableView2().refresh();
            System.out.println("step2 table is refreshed");
        });
        refreshButton3.setOnAction(e -> {
            getSlidesTableView3().refresh();
            System.out.println("step3 table is refreshed");
        });
        refreshButton4.setOnAction(e -> {
            getSlidesTableView4().refresh();
            System.out.println("step4 table is refreshed");
        });
        refreshButton5.setOnAction(e -> {
            getSlidesTableView5().refresh();
            System.out.println("step5 table is refreshed");
        });

        viewButton.setOnAction(e -> {
            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            Slide selectedSlide = workspace.getSlidesTableView().getSelectionModel().getSelectedItem();
            SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();

            Alert alert = new Alert(AlertType.INFORMATION);
            String infor = selectedSlide.getPath();
            String pathString = "./work/" + app.getGUI().getAppTitle() + "/code/" + selectedSlide.getFileName();
            alert.setTitle("File Information");
            alert.setHeaderText(null);
            alert.setContentText("The file is in " + pathString);
            alert.showAndWait();
        });

        viewButton2.setOnAction(e -> {
            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            Slide selectedSlide = workspace.getSlidesTableView2().getSelectionModel().getSelectedItem();
            SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();

            Alert alert = new Alert(AlertType.INFORMATION);
            String infor = selectedSlide.getPath();
            alert.setTitle("File Information");
            alert.setHeaderText(null);
            String pathString = "./work/" + app.getGUI().getAppTitle() + "/code/" + selectedSlide.getFileName();
            alert.setContentText("The file is in " + pathString);
            alert.showAndWait();
        });

        viewButton3.setOnAction(e -> {
            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            Slide selectedSlide = workspace.getSlidesTableView3().getSelectionModel().getSelectedItem();
            SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();

            Alert alert = new Alert(AlertType.INFORMATION);
            String infor = selectedSlide.getPath();
            alert.setTitle("File Information");
            alert.setHeaderText(null);
            String pathString = "./work/" + app.getGUI().getAppTitle() + "/code/" + selectedSlide.getFileName();
            alert.setContentText("The file is in " + pathString);
            alert.showAndWait();
        });

        viewButton4.setOnAction(e -> {
            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            Slide selectedSlide = workspace.getSlidesTableView4().getSelectionModel().getSelectedItem();
            SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();

            Alert alert = new Alert(AlertType.INFORMATION);
            String infor = selectedSlide.getPath();
            alert.setTitle("File Information");
            alert.setHeaderText(null);
            String pathString = "./work/" + app.getGUI().getAppTitle() + "/code/" + selectedSlide.getFileName();
            alert.setContentText("The file is in " + pathString);
            alert.showAndWait();
        });

        viewButton5.setOnAction(e -> {
            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            Slide selectedSlide = workspace.getSlidesTableView5().getSelectionModel().getSelectedItem();
            SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();

            Alert alert = new Alert(AlertType.INFORMATION);
            String infor = selectedSlide.getPath();
            alert.setTitle("File Information");
            alert.setHeaderText(null);
            String pathString = "./work/" + app.getGUI().getAppTitle() + "/code/" + selectedSlide.getFileName();
            alert.setContentText("The file is in " + pathString);
            alert.showAndWait();
        });

        getPreviousButton().setOnAction(e -> {
//            controller.handlePreviousScene(getStep());
//            setStep(getStep() - 1);
            controller.handleStepScene(getStep() - 1);
            setStep(getStep() - 1);
        });

        getSlidesTableView().getSelectionModel().selectedItemProperty().addListener(e -> {
            controller.handleSelectSlide();
        });

        getCaptionTextField().textProperty().addListener(e -> {
            controller.handleCaptionTextTyped();
        });

//        extractButton.setOnAction(e -> {
//           System.out.println("extractbutton clicked");
//
//            task.playFromStart();
//
//            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
//            Slide selectedSlide = workspace.getSlidesTableView().getSelectionModel().getSelectedItem();
//            int selectedIndex = workspace.getSlidesTableView().getSelectionModel().getSelectedIndex();
//            // File file=new File();
//            // WE ONLY RESPOND IF IT'S A SELECTION
//            if (selectedIndex >= 0) {
//                try {
//                    // LOAD ALL THE SLIDE D
//
//                    controller.extraZip(selectedSlide.getPath());
//
//                } catch (ZipException ex) {
//                    Logger.getLogger(SlideshowCreatorWorkspace.class.getName()).log(Level.SEVERE, null, ex);
//                } catch (IOException ex) {
//                    Logger.getLogger(SlideshowCreatorWorkspace.class.getName()).log(Level.SEVERE, null, ex);
//                }
//
//            };
//
//        }); 
        getExtractButton().setOnAction(new EventHandler<ActionEvent>() {
            //  System.out.println("extractbutton clicked");
            @Override
            public void handle(ActionEvent event) {

                SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
                Slide selectedSlide = workspace.getSlidesTableView().getSelectionModel().getSelectedItem();
                int selectedIndex = workspace.getSlidesTableView().getSelectionModel().getSelectedIndex();

                // File file=new File();
                // WE ONLY RESPOND IF IT'S A SELECTION
                if (selectedIndex >= 0) {
                    try {
                        // LOAD ALL THE SLIDE D

                        controller.extraZip(selectedSlide.getPath());

                    } catch (ZipException ex) {
                        Logger.getLogger(SlideshowCreatorWorkspace.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(SlideshowCreatorWorkspace.class.getName()).log(Level.SEVERE, null, ex);
                    }

                };

                getExtractBar().setProgress(0);
                getExtractIndicator().setProgress(0);

                // Create a Task.
                copyTask = new CopyTask();

                copyTask.setFilePath("./work");
                // Unbind progress property
                getExtractBar().progressProperty().unbind();

                // Bind progress property
                getExtractBar().progressProperty().bind(copyTask.progressProperty());

                // Hủy bỏ kết nối thuộc tính progress
                getExtractIndicator().progressProperty().unbind();

                // Bind progress property.
                getExtractIndicator().progressProperty().bind(copyTask.progressProperty());

                // Start the Task.
                new Thread(copyTask).start();

            }

        });

        renameButton.setOnAction(e -> {
            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            //Slide selectedSlide = workspace.getSlidesTableView2().getSelectionModel().getSelectedItem();
            int selectedIndex = workspace.getSlidesTableView2().getSelectionModel().getSelectedIndex();
            renameButton.setDisable(false);

            // File file=new File();
            // WE ONLY RESPOND IF IT'S A SELECTION
            //if (selectedIndex >= 0) {
                // LOAD ALL THE SLIDE D
                controller.renameCurrentTable(workspace.getSlidesTableView2());
                
                
            //};
            
            getRenameBar().setProgress(0);
                getRenameIndicator().setProgress(0);

                // Create a Task.
                copyTask1 = new CopyTask1();

                copyTask1.setFilePath("./work");
                // Unbind progress property
                getRenameBar().progressProperty().unbind();

                // Bind progress property
                getRenameBar().progressProperty().bind(copyTask1.progressProperty());

                // Hủy bỏ kết nối thuộc tính progress
                getRenameIndicator().progressProperty().unbind();

                // Bind progress property.
                getRenameIndicator().progressProperty().bind(copyTask1.progressProperty());

                // Start the Task.
                new Thread(copyTask1).start();
        });

        unzipButton.setOnAction(e -> {
            String textFiled = "Successfully unzipped files:\n";
            String extractFailString = "\nUnzip Errors:\n";

            codeButton.setDisable(false);
            //System.out.println("unzip--------");
            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
            Slide selectedSlide = workspace.getSlidesTableView3().getSelectionModel().getSelectedItem();
            int selectedIndex = workspace.getSlidesTableView3().getSelectionModel().getSelectedIndex();

            String path = "./work/" + app.getGUI().getAppTitle() + "/submissions/" + selectedSlide.getFileName();
            String destPath = "./work/" + app.getGUI().getAppTitle() + "/projects";

            // File file=new File();
            // WE ONLY RESPOND IF IT'S A SELECTION
            if (selectedIndex >= 0) {
                try {
                    controller.unZipFiles(new File(path), destPath);
//                    File orgFile=new File(destPath);
//                    String newNameString=destPath+orgFile.getName();

                    File outsideFile = new File(destPath);
                    File[] insideFile = outsideFile.listFiles();
                    File fileWithNewName = new File(insideFile[0].getPath());
                    File newName = new File(destPath + "/" + selectedSlide.getFileName().substring(0, selectedSlide.getFileName().lastIndexOf(".")));
                    if (fileWithNewName.isDirectory()) {
                        fileWithNewName.renameTo(newName);

                        textFiled += "-" + selectedSlide.getFileName() + "\n";

                    } else {
                        fileWithNewName.mkdir();
                        fileWithNewName.renameTo(newName);
                    }

                    Slide slide = new Slide(newName.getName(), destPath + "/" + selectedSlide.getFileName().substring(0, selectedSlide.getFileName().lastIndexOf(".")), "");
                    data.getSlide4().add(slide);

                } catch (IOException eee) {
                    eee.printStackTrace();
                }
                workspace.unzipTextField.setEditable(false);
                workspace.unzipTextField.setText(textFiled + extractFailString);
            };
            
            getUnzipBar().setProgress(0);
                getUnzipIndicator().setProgress(0);

                // Create a Task.
                copyTask2 = new CopyTask2();

                copyTask2.setFilePath("./work");
                // Unbind progress property
                getUnzipBar().progressProperty().unbind();

                // Bind progress property
                getUnzipBar().progressProperty().bind(copyTask2.progressProperty());

                // Hủy bỏ kết nối thuộc tính progress
                getUnzipIndicator().progressProperty().unbind();

                // Bind progress property.
                getUnzipIndicator().progressProperty().bind(copyTask2.progressProperty());

                // Start the Task.
                new Thread(copyTask2).start();
        });

        codeButton.setOnAction(e -> {
            SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();

            Slide selectedSlide = workspace.getSlidesTableView4().getSelectionModel().getSelectedItem();
            File codeFile = new File("./work/" + app.getGUI().getAppTitle() + "/code/"
                    + selectedSlide.getFileName());
            if (!codeFile.isDirectory()) {
                codeFile.mkdir();
            }
            String path = selectedSlide.getPath();
            String str = null;
            if (javaBox.isSelected()) {
                str = "java";
            } else if (jsBox.isSelected()) {
                str = "js";
            } else if (csBox.isSelected()) {
                str = "cs";
            } else if (other.isSelected()) {
                str = otherField.getText();
            }
            System.out.println("in the other field------------" + otherField.getText());
            controller.handleExtractCode(str, path, codeFile);

            if (chcppBox.isSelected()) {
                controller.handleExtractCode("c", "h", "cpp", path, codeFile);
            }

            Slide slide = new Slide(selectedSlide.getFileName(),
                    codeFile.getAbsolutePath(), "");
            data.getSlide5().add(slide);
            checkButton.setDisable(false);
            resultButton.setDisable(false);
            
            getCodeBar().setProgress(0);
                getCodeIndicator().setProgress(0);

                // Create a Task.
                copyTask3 = new CopyTask3();

                copyTask3.setFilePath("./work");
                // Unbind progress property
                getCodeBar().progressProperty().unbind();

                // Bind progress property
                getCodeBar().progressProperty().bind(copyTask3.progressProperty());

                // Hủy bỏ kết nối thuộc tính progress
                getCodeIndicator().progressProperty().unbind();

                // Bind progress property.
                getCodeIndicator().progressProperty().bind(copyTask3.progressProperty());

                // Start the Task.
                new Thread(copyTask3).start();
                
                checkButton.setDisable(false);

        });

        checkButton.setOnAction(e -> {
String resultString = "Student Plaglarism Check results can be found at\n";
            resultString += "http://www.google.com";
             resultButton.setDisable(false);           
           
            resultTextField.setText(resultString);
            
                getFinalBar().setProgress(0);
                getFinalIndicator().setProgress(0);

                // Create a Task.
                copyTask4 = new CopyTask4();

                copyTask4.setFilePath("./work");
                // Unbind progress property
                getFinalBar().progressProperty().unbind();

                // Bind progress property
                getFinalBar().progressProperty().bind(copyTask4.progressProperty());

                // Hủy bỏ kết nối thuộc tính progress
                getFinalIndicator().progressProperty().unbind();

                // Bind progress property.
                getFinalIndicator().progressProperty().bind(copyTask4.progressProperty());

                // Start the Task.
                new Thread(copyTask4).start();
            resultTextField.setEditable(false);
            
            

        });
        
        
        resultButton.setOnAction(e -> {
        
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("http://www.google.com");
            alert.setHeaderText(null);
            WebView webView = new WebView();
            webView.getEngine().loadContent("<html>\n" +
                    "<body>\n" +
                    "\n" +
                    "<h1>Welcome to 219</h1>\n" +
                    "\n" +
                    "<p>I'm so exicted, 219 is fianlly over now!!!!</p >\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>");
            webView.setPrefSize(600, 600);
            alert.getDialogPane().setContent(webView);;
            alert.showAndWait();
            
        });
    }
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT

    private void initStyle() {

        editToolbar.getStyleClass().add(CLASS_BORDERED_PANE);
        multiplePanes.getStyleClass().add(CLASS_NONBORDERED_PANE);

        multiplePanes2.getStyleClass().add(CLASS_NONBORDERED_PANE);
        multiplePanes3.getStyleClass().add(CLASS_NONBORDERED_PANE);
        multiplePanes4.getStyleClass().add(CLASS_NONBORDERED_PANE);
        multiplePanes5.getStyleClass().add(CLASS_NONBORDERED_PANE);

        step5Box.getStyleClass().add(CLASS_NONBORDERED_PANE);
        srcTypeLabel.getStyleClass().add(CLASS_SENTENCE_LABEL);

        bar1.getStyleClass().add(CLASS_NONBORDERED_PANE);
        getExtractBar().getStyleClass().add(PROGRESS_BAR);
        bar2.getStyleClass().add(CLASS_NONBORDERED_PANE);
        getRenameBar().getStyleClass().add(PROGRESS_BAR);
        bar3.getStyleClass().add(CLASS_NONBORDERED_PANE);
        getUnzipBar().getStyleClass().add(PROGRESS_BAR);
        bar4.getStyleClass().add(CLASS_NONBORDERED_PANE);
        getCodeBar().getStyleClass().add(PROGRESS_BAR);
        bar5.getStyleClass().add(CLASS_NONBORDERED_PANE);
        getFinalBar().getStyleClass().add(PROGRESS_BAR);

        getPreviousButton().getStyleClass().add(CLASS_EDIT_BUTTON);
        homeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        getNextButton().getStyleClass().add(CLASS_EDIT_BUTTON);

        removeButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        removeButton2.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton2.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton2.getStyleClass().add(CLASS_EDIT_BUTTON);

        removeButton3.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton3.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton3.getStyleClass().add(CLASS_EDIT_BUTTON);

        removeButton4.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton4.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton4.getStyleClass().add(CLASS_EDIT_BUTTON);

        removeButton5.getStyleClass().add(CLASS_EDIT_BUTTON);
        refreshButton5.getStyleClass().add(CLASS_EDIT_BUTTON);
        viewButton5.getStyleClass().add(CLASS_EDIT_BUTTON);

        checkButton.getStyleClass().add(CLASS_EDIT_BUTTON);
        resultButton.getStyleClass().add(CLASS_EDIT_BUTTON);

        // THE SLIDES TABLE
        getSlidesTableView().getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : getSlidesTableView().getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        slidesTableView2.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView2.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        slidesTableView3.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView3.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        slidesTableView4.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView4.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        slidesTableView5.getStyleClass().add(CLASS_SLIDES_TABLE);
        for (TableColumn tc : slidesTableView5.getColumns()) {
            tc.getStyleClass().add(CLASS_SLIDES_TABLE);
        }

        getEditPane().getStyleClass().add(CLASS_SLIDES_TABLE);
        getEditPane().getStyleClass().add(CLASS_BORDERED_PANE);
        getExtractPane().getStyleClass().add(CLASS_BORDERED_PANE);
        etrPrgLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        step1.getStyleClass().add(CLASS_PROMPT_LABEL);
        step1SentenceLabel.getStyleClass().add(CLASS_SENTENCE_LABEL);
        getCaptionTextField().getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        getExtractButton().getStyleClass().add(CLASS_UPDATE_BUTTON);

        //rename
        getEditPane2().getStyleClass().add(CLASS_SLIDES_TABLE);
        getEditPane2().getStyleClass().add(CLASS_BORDERED_PANE);
        getExtractPane2().getStyleClass().add(CLASS_BORDERED_PANE);
        getRenPrgLabel().getStyleClass().add(CLASS_PROMPT_LABEL);
        getStep2().getStyleClass().add(CLASS_PROMPT_LABEL);
        getStepSentenceLabe2().getStyleClass().add(CLASS_SENTENCE_LABEL);
        getRenameTextField().getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        getRenameButton().getStyleClass().add(CLASS_UPDATE_BUTTON);

        getEditPane3().getStyleClass().add(CLASS_SLIDES_TABLE);
        getEditPane3().getStyleClass().add(CLASS_BORDERED_PANE);
        getExtractPane3().getStyleClass().add(CLASS_BORDERED_PANE);
        getUnzipPrgLabel().getStyleClass().add(CLASS_PROMPT_LABEL);
        getStep3().getStyleClass().add(CLASS_PROMPT_LABEL);
        getStepSentenceLabe3().getStyleClass().add(CLASS_SENTENCE_LABEL);
        getUnzipTextField().getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        getUnzipButton().getStyleClass().add(CLASS_UPDATE_BUTTON);

        getEditPane4().getStyleClass().add(CLASS_SLIDES_TABLE);
        getEditPane4().getStyleClass().add(CLASS_BORDERED_PANE);
        getExtractPane4().getStyleClass().add(CLASS_BORDERED_PANE);
        getCodePrgLabel().getStyleClass().add(CLASS_PROMPT_LABEL);
        getStep4().getStyleClass().add(CLASS_PROMPT_LABEL);
        getStepSentenceLabe4().getStyleClass().add(CLASS_SENTENCE_LABEL);
        getCodeTextField().getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        getCodeButton().getStyleClass().add(CLASS_UPDATE_BUTTON);

        getEditPane5().getStyleClass().add(CLASS_SLIDES_TABLE);
        getEditPane5().getStyleClass().add(CLASS_BORDERED_PANE);
        getExtractPane5().getStyleClass().add(CLASS_BORDERED_PANE);
        getCheckPrgLabel().getStyleClass().add(CLASS_PROMPT_LABEL);
        getStep5().getStyleClass().add(CLASS_PROMPT_LABEL);
        getStepSentenceLabe5().getStyleClass().add(CLASS_SENTENCE_LABEL);
        getResultTextField().getStyleClass().add(CLASS_EDIT_TEXT_FIELD);
        getResultButton().getStyleClass().add(CLASS_UPDATE_BUTTON);
        getCheckButton().getStyleClass().add(CLASS_UPDATE_BUTTON);

    }

    @Override
    public void resetWorkspace() {
        getPreviousButton().setDisable(true);
        homeButton.setDisable(true);
        getNextButton().setDisable(true);
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
        data.resetData();
        enableSlideEditingControls(false);
        enableCheckBox(false);
    }

    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        getExtractButton().setDisable(true);
        renameButton.setDisable(true);
        unzipButton.setDisable(true);
        codeButton.setDisable(true);
        checkButton.setDisable(true);
        resultButton.setDisable(true);

        refreshButton.setDisable(false);
        refreshButton2.setDisable(false);
        refreshButton3.setDisable(false);
        refreshButton4.setDisable(false);
        refreshButton5.setDisable(false);

        removeButton.setDisable(false);
        removeButton2.setDisable(false);
        removeButton3.setDisable(false);
        removeButton4.setDisable(false);
        removeButton5.setDisable(false);

        viewButton.setDisable(false);
        viewButton2.setDisable(false);
        viewButton3.setDisable(false);
        viewButton4.setDisable(false);
        viewButton5.setDisable(false);

    }

    public void enableCheckBox(boolean enable) {
        javaBox.setDisable(!enable);
        jsBox.setDisable(!enable);
        chcppBox.setDisable(!enable);
        csBox.setDisable(!enable);
        other.setDisable(!enable);
        otherField.setDisable(!enable);
    }

    public void enableSlideEditingControls(boolean enable) {
        // ENABLE THINGS THAT CAN BE USED
        getCaptionTextField().setDisable(!enable);
        renameTextField.setDisable(!enable);
        unzipTextField.setDisable(!enable);
        codeTextField.setDisable(!enable);
        resultTextField.setDisable(!enable);

//        currentWidthSlider.setDisable(!enable);
//        currentHeightSlider.setDisable(!enable);
//        updateButton.setDisable(enable);
        getNextButton().setDisable(!enable);
//        previousButton.setDisable(enable);

        // SHOULD WE CLEAR THE DATA TOO?
        if (!enable) {
//            fileNameTextField.setText("");
//            pathTextField.setText("");
            getCaptionTextField().setText("");
//            originalWidthTextField.setText("");
//            originalHeightTextField.setText("");
//            currentWidthSlider.setValue(0);
//            currentHeightSlider.setValue(0);
        }
    }

    /**
     * @return the editPane
     */
    public GridPane getEditPane() {
        return editPane;
    }

    /**
     * @return the extractPane
     */
    public GridPane getExtractPane() {
        return extractPane;
    }

    /**
     * @return the workspaceBorderPane
     */
    public GridPane getWorkspaceBorderPane() {
        return workspaceBorderPane;
    }

    /**
     * @return the step2
     */
    public Label getStep2() {
        return step2;
    }

    /**
     * @return the editPane2
     */
    public GridPane getEditPane2() {

        return editPane2;

    }

    /**
     * @return the extractPane2
     */
    public GridPane getExtractPane2() {

        return extractPane2;
    }

    /**
     * @return the editPane3
     */
    public GridPane getEditPane3() {

        return editPane3;
    }

    /**
     * @return the extractPane3
     */
    public GridPane getExtractPane3() {
        return extractPane3;
    }

    /**
     * @return the editPane4
     */
    public GridPane getEditPane4() {

        return editPane4;
    }

    /**
     * @return the extractPane4
     */
    public GridPane getExtractPane4() {
        return extractPane4;
    }

    /**
     * @return the editPane5
     */
    public GridPane getEditPane5() {

        return editPane5;
    }

    /**
     * @return the extractPane5
     */
    public GridPane getExtractPane5() {
        return extractPane5;
    }

    /**
     * @return the stepSentenceLabe2
     */
    public Label getStepSentenceLabe2() {
        return stepSentenceLabe2;
    }

    /**
     * @return the renPrgLabel
     */
    public Label getRenPrgLabel() {
        return renPrgLabel;
    }

    /**
     * @return the renameButton
     */
    public Button getRenameButton() {
        return renameButton;
    }

    /**
     * @return the renameTextField
     */
    public TextArea getRenameTextField() {
        return renameTextField;
    }

    /**
     * @return the slidesTableView2
     */
    public TableView<Slide> getSlidesTableView2() {
        return slidesTableView2;
    }

    /**
     * @return the fileNameColum2
     */
    public TableColumn<Slide, StringProperty> getFileNameColum2() {
        return fileNameColum2;
    }

    /**
     * @return the step3
     */
    public Label getStep3() {
        return step3;
    }

    /**
     * @return the stepSentenceLabe3
     */
    public Label getStepSentenceLabe3() {
        return stepSentenceLabe3;
    }

    /**
     * @return the unzipPrgLabel
     */
    public Label getUnzipPrgLabel() {
        return unzipPrgLabel;
    }

    /**
     * @return the unzipButton
     */
    public Button getUnzipButton() {
        return unzipButton;
    }

    /**
     * @return the unzipTextField
     */
    public TextArea getUnzipTextField() {
        return unzipTextField;
    }

    /**
     * @return the slidesTableView3
     */
    public TableView<Slide> getSlidesTableView3() {
        return slidesTableView3;
    }

    /**
     * @return the fileNameColum3
     */
    public TableColumn<Slide, StringProperty> getFileNameColum3() {
        return fileNameColum3;
    }

    /**
     * @return the step4
     */
    public Label getStep4() {
        return step4;
    }

    /**
     * @return the stepSentenceLabe4
     */
    public Label getStepSentenceLabe4() {
        return stepSentenceLabe4;
    }

    /**
     * @return the codePrgLabel
     */
    public Label getCodePrgLabel() {
        return codePrgLabel;
    }

    /**
     * @return the codeButton
     */
    public Button getCodeButton() {
        return codeButton;
    }

    /**
     * @return the codeTextField
     */
    public TextArea getCodeTextField() {
        return codeTextField;
    }

    /**
     * @return the slidesTableView4
     */
    public TableView<Slide> getSlidesTableView4() {
        return slidesTableView4;
    }

    /**
     * @return the fileNameColum4
     */
    public TableColumn<Slide, StringProperty> getFileNameColum4() {
        return fileNameColum4;
    }

    /**
     * @return the step5
     */
    public Label getStep5() {
        return step5;
    }

    /**
     * @return the stepSentenceLabe5
     */
    public Label getStepSentenceLabe5() {
        return stepSentenceLabe5;
    }

    /**
     * @return the checkPrgLabel
     */
    public Label getCheckPrgLabel() {
        return checkPrgLabel;
    }

    /**
     * @return the checkButton
     */
    public Button getCheckButton() {
        return checkButton;
    }

    /**
     * @return the resultButton
     */
    public Button getResultButton() {
        return resultButton;
    }

    /**
     * @return the resultTextField
     */
    public TextArea getResultTextField() {
        return resultTextField;
    }

    /**
     * @return the slidesTableView5
     */
    public TableView<Slide> getSlidesTableView5() {
        return slidesTableView5;
    }

    /**
     * @return the fileNameColum5
     */
    public TableColumn<Slide, StringProperty> getFileNameColum5() {
        return fileNameColum5;
    }

    /**
     * @return the step
     */
    public static int getStep() {
        return step;
    }

    /**
     * @param aStep the step to set
     */
    public static void setStep(int aStep) {
        step = aStep;
    }

    /**
     * @return the slidesTableView
     */
    public TableView<Slide> getSlidesTableView() {
        return slidesTableView;
    }

    /**
     * @return the previousButton
     */
    public Button getPreviousButton() {
        return previousButton;
    }

    /**
     * @return the nextButton
     */
    public Button getNextButton() {
        return nextButton;
    }

    /**
     * @return the extractButton
     */
    public Button getExtractButton() {
        return extractButton;
    }

    /**
     * @param extractButton the extractButton to set
     */
    public void setExtractButton(Button extractButton) {
        this.extractButton = extractButton;
    }

    /**
     * @return the filesTableScrollPane5
     */
    public ScrollPane getFilesTableScrollPane5() {
        return filesTableScrollPane5;
    }

    /**
     * @param filesTableScrollPane5 the filesTableScrollPane5 to set
     */
    public void setFilesTableScrollPane5(ScrollPane filesTableScrollPane5) {
        this.filesTableScrollPane5 = filesTableScrollPane5;
    }

    /**
     * @return the textPane5
     */
    public ScrollPane getTextPane5() {
        return textPane5;
    }

    /**
     * @param textPane5 the textPane5 to set
     */
    public void setTextPane5(ScrollPane textPane5) {
        this.textPane5 = textPane5;
    }

    /**
     * @return the extractBar
     */
    public ProgressBar getExtractBar() {
        return extractBar;
    }

    /**
     * @param extractBar the extractBar to set
     */
    public void setExtractBar(ProgressBar extractBar) {
        this.extractBar = extractBar;
    }

    /**
     * @return the extractIndicator
     */
    public ProgressIndicator getExtractIndicator() {
        return extractIndicator;
    }

    /**
     * @param extractIndicator the extractIndicator to set
     */
    public void setExtractIndicator(ProgressIndicator extractIndicator) {
        this.extractIndicator = extractIndicator;
    }

    /**
     * @return the renameBar
     */
    public ProgressBar getRenameBar() {
        return renameBar;
    }

    /**
     * @param renameBar the renameBar to set
     */
    public void setRenameBar(ProgressBar renameBar) {
        this.renameBar = renameBar;
    }

    /**
     * @return the renameIndicator
     */
    public ProgressIndicator getRenameIndicator() {
        return renameIndicator;
    }

    /**
     * @param renameIndicator the renameIndicator to set
     */
    public void setRenameIndicator(ProgressIndicator renameIndicator) {
        this.renameIndicator = renameIndicator;
    }

    /**
     * @return the unzipBar
     */
    public ProgressBar getUnzipBar() {
        return unzipBar;
    }

    /**
     * @param unzipBar the unzipBar to set
     */
    public void setUnzipBar(ProgressBar unzipBar) {
        this.unzipBar = unzipBar;
    }

    /**
     * @return the unzipIndicator
     */
    public ProgressIndicator getUnzipIndicator() {
        return unzipIndicator;
    }

    /**
     * @param unzipIndicator the unzipIndicator to set
     */
    public void setUnzipIndicator(ProgressIndicator unzipIndicator) {
        this.unzipIndicator = unzipIndicator;
    }

    /**
     * @return the codeBar
     */
    public ProgressBar getCodeBar() {
        return codeBar;
    }

    /**
     * @param codeBar the codeBar to set
     */
    public void setCodeBar(ProgressBar codeBar) {
        this.codeBar = codeBar;
    }

    /**
     * @return the codeIndicator
     */
    public ProgressIndicator getCodeIndicator() {
        return codeIndicator;
    }

    /**
     * @param codeIndicator the codeIndicator to set
     */
    public void setCodeIndicator(ProgressIndicator codeIndicator) {
        this.codeIndicator = codeIndicator;
    }

    /**
     * @return the finalBar
     */
    public ProgressBar getFinalBar() {
        return finalBar;
    }

    /**
     * @param finalBar the finalBar to set
     */
    public void setFinalBar(ProgressBar finalBar) {
        this.finalBar = finalBar;
    }

    /**
     * @return the finalIndicator
     */
    public ProgressIndicator getFinalIndicator() {
        return finalIndicator;
    }

    /**
     * @param finalIndicator the finalIndicator to set
     */
    public void setFinalIndicator(ProgressIndicator finalIndicator) {
        this.finalIndicator = finalIndicator;
    }

    /**
     * @return the captionTextField
     */
    public TextArea getCaptionTextField() {
        return captionTextField;
    }

}
