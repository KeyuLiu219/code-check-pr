/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sc.workspace;

/**
 *
 * @author Sean Zhu
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.zip.ZipException;
 
import javafx.concurrent.Task;
import sc.SlideshowCreatorApp;
import sc.data.Slide;
 
// Copy all file in C:/Windows
public class CopyTask extends Task<List<File>> {
 
    private String filePath;
    private SlideshowCreatorApp app;
    private SlideshowCreatorController controller;
    @Override
    protected List<File> call() throws Exception {
             
 
        File dir = new File(getFilePath());
        System.out.println(filePath);
        File[] files = dir.listFiles();
        for(int i = 0; i < files.length; i++){
            System.out.println(files[i].getName());
        }
        int count = files.length;
 
        List<File> copied = new ArrayList<File>();
        int i = 0;
        for (File file : files) {
            if (file.isFile()) {
                //this.copy(file);
                 this.updateMessage("Copying: " + file.getAbsolutePath());
                Thread.sleep(100);
                copied.add(file);
            }
            i++;
            this.updateProgress(i, count);
        }
        return copied;
    }


    /**
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * @param filePath the filePath to set
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * @return the app
     */
    public SlideshowCreatorApp getApp() {
        return app;
    }

    /**
     * @param app the app to set
     */
    public void setApp(SlideshowCreatorApp app) {
        this.app = app;
    }

    /**
     * @return the controller
     */
    public SlideshowCreatorController getController() {
        return controller;
    }

    /**
     * @param controller the controller to set
     */
    public void setController(SlideshowCreatorController controller) {
        this.controller = controller;
    }
    
    
 
}
