package sc.workspace;

import djf.ui.AppMessageDialogSingleton;
import java.io.BufferedOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.ZipException;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import properties_manager.PropertiesManager;
import sc.SlideshowCreatorApp;
import static sc.SlideshowCreatorProp.APP_PATH_WORK;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_MESSAGE;
import static sc.SlideshowCreatorProp.INVALID_IMAGE_PATH_TITLE;
import sc.data.Slide;
import sc.data.SlideshowCreatorData;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import quicktime.app.spaces.Controller;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file toolbar.
 *
 * @author Richard McKenna & Keyu LIu
 * @version 1.0
 */
public class SlideshowCreatorController {

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    SlideshowCreatorApp app;
    private static final int BUFFER_SIZE = 4096;
    private static final String FILE_PATH = "./work/";
    //   private String FILE_PATH = "./work/"+app.getGUI().getPrimaryStage().getTitle()+"/submissions/";

    /**
     * Constructor, note that the app must already be constructed.
     */
    public SlideshowCreatorController(SlideshowCreatorApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }

    // CONTROLLER METHOD THAT HANDLES ADDING A DIRECTORY OF IMAGES
    public void handleAddAllImagesInDirectory() {
        try {
            // ASK THE USER TO SELECT A DIRECTORY
            DirectoryChooser dirChooser = new DirectoryChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            dirChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File dir = dirChooser.showDialog(app.getGUI().getWindow());
            if (dir != null) {
                File[] files = dir.listFiles();
                for (File f : files) {
                    String fileName = f.getName();
                    if (fileName.toLowerCase().endsWith(".png")
                            || fileName.toLowerCase().endsWith(".jpg")
                            || fileName.toLowerCase().endsWith(".gif")) {
                        String path = f.getPath();
                        String caption = "";
                        Image slideShowImage = loadImage(path);
                        int originalWidth = (int) slideShowImage.getWidth();
                        int originalHeight = (int) slideShowImage.getHeight();
                        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                        data.addNonDuplicateSlide(fileName, path, caption, originalWidth, originalHeight);
                    }
                }
            }
        } catch (MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
    }

    public void handleAddImage() {
        try {
            // ASK THE USER TO SELECT A DIRECTORY
            FileChooser imageChooser = new FileChooser();
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            imageChooser.setInitialDirectory(new File(props.getProperty(APP_PATH_WORK)));
            File imageFile = imageChooser.showOpenDialog(app.getGUI().getWindow());
            if (imageFile != null) {
                String fileName = imageFile.getName();
                if (fileName.toLowerCase().endsWith(".png")
                        || fileName.toLowerCase().endsWith(".jpg")
                        || fileName.toLowerCase().endsWith(".gif")) {
                    String path = imageFile.getPath();
                    String caption = "";
                    Image slideShowImage = loadImage(path);
                    int originalWidth = (int) slideShowImage.getWidth();
                    int originalHeight = (int) slideShowImage.getHeight();
                    SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
                    data.addNonDuplicateSlide(fileName, path, caption, originalWidth, originalHeight);
                }
            }
        } catch (MalformedURLException murle) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String title = props.getProperty(INVALID_IMAGE_PATH_TITLE);
            String message = props.getProperty(INVALID_IMAGE_PATH_MESSAGE);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(title, message);
        }
    }

    public void homePage() {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        GridPane bigPane;

        workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        bigPane = (GridPane) app.getWorkspaceComponent().getWorkspace();
        bigPane.getChildren().clear();

        bigPane.add(workspace.getExtractPane(), 0, 0);
        bigPane.add(workspace.getEditPane(), 1, 0);
    }

    public void handleStepScene(int setp) {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        GridPane bigPane;

        Slide selectedSlide = workspace.getSlidesTableView().getSelectionModel().getSelectedItem();
        int selectedIndex = workspace.getSlidesTableView().getSelectionModel().getSelectedIndex();

        if (setp == 1) {
            workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            bigPane = (GridPane) app.getWorkspaceComponent().getWorkspace();
            bigPane.getChildren().clear();
            bigPane.add(workspace.getExtractPane(), 0, 0);
            bigPane.add(workspace.getEditPane(), 1, 0);
            workspace.getNextButton().setDisable(false);
            workspace.homeButton.setDisable(true);
            workspace.getPreviousButton().setDisable(true);

        } else if (setp == 2) {
            workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            bigPane = (GridPane) app.getWorkspaceComponent().getWorkspace();
            bigPane.getChildren().clear();
            bigPane.add(workspace.getExtractPane2(), 0, 0);
            bigPane.add(workspace.getEditPane2(), 1, 0);
            workspace.getNextButton().setDisable(false);
            workspace.homeButton.setDisable(false);
            workspace.getPreviousButton().setDisable(false);

        } else if (setp == 3) {
            workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            bigPane = (GridPane) app.getWorkspaceComponent().getWorkspace();
            bigPane.getChildren().clear();

            bigPane.add(workspace.getExtractPane3(), 0, 0);
            bigPane.add(workspace.getEditPane3(), 1, 0);
            workspace.getNextButton().setDisable(false);
            workspace.homeButton.setDisable(false);
            workspace.getPreviousButton().setDisable(false);

            workspace.getSlidesTableView3().refresh();
            workspace.getUnzipButton().setDisable(false);

        } else if (setp == 4) {
            workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            bigPane = (GridPane) app.getWorkspaceComponent().getWorkspace();
            bigPane.getChildren().clear();

            bigPane.add(workspace.getExtractPane4(), 0, 0);
            bigPane.add(workspace.getEditPane4(), 1, 0);
            workspace.getNextButton().setDisable(false);
            workspace.homeButton.setDisable(false);
            workspace.getPreviousButton().setDisable(false);

            //workspace.getUnzipButton().setDisable(false);
        } else if (setp == 5) {
            workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
            bigPane = (GridPane) app.getWorkspaceComponent().getWorkspace();
            bigPane.getChildren().clear();

            bigPane.add(workspace.getExtractPane5(), 0, 0);
            workspace.homeButton.setDisable(false);
            bigPane.add(workspace.getEditPane5(), 1, 0);
            workspace.getNextButton().setDisable(true);

            workspace.getPreviousButton().setDisable(false);
        }

    }

    public void handleUpdateSlide() {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        workspace.getExtractButton().setDisable(true);

        TableView slidesTableView = workspace.getSlidesTableView();
        Slide selectedSlide = (Slide) slidesTableView.getSelectionModel().getSelectedItem();
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();

        String caption = workspace.getCaptionTextField().getText();
        selectedSlide.setCaption(caption);
        workspace.getSlidesTableView().refresh();
    }

    public void handleSelectSlide() {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        Slide selectedSlide = workspace.getSlidesTableView().getSelectionModel().getSelectedItem();
        int selectedIndex = workspace.getSlidesTableView().getSelectionModel().getSelectedIndex();

        // WE ONLY RESPOND IF IT'S A SELECTION
        if (selectedIndex >= 0) {
            // LOAD ALL THE SLIDE DATA INTO THE CONTROLS
//            workspace.fileNameTextField.setText(selectedSlide.getFileName());
//            workspace.pathTextField.setText(selectedSlide.getPath());
//            workspace.captionTextField.setText(selectedSlide.getCaption());
//            workspace.originalWidthTextField.setText("" + selectedSlide.getOriginalWidth());
//            workspace.originalHeightTextField.setText("" + selectedSlide.getOriginalHeight());
//            workspace.currentWidthSlider.setValue(selectedSlide.getCurrentWidth().doubleValue());
//            workspace.currentHeightSlider.setValue(selectedSlide.getCurrentHeight().doubleValue());

            workspace.enableSlideEditingControls(true);
            workspace.enableCheckBox(true);
        }
    }

    // THIS HELPER METHOD LOADS AN IMAGE SO WE CAN SEE IT'S SIZE
    private Image loadImage(String imagePath) throws MalformedURLException {
        File file = new File(imagePath);
        URL fileURL = file.toURI().toURL();
        Image image = new Image(fileURL.toExternalForm());
        return image;
    }

    void handleCaptionTextTyped() {
        SlideshowCreatorWorkspace gui = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        gui.getExtractButton().setDisable(false);
    }

    public void unZipFiles(String zipPath, String descDir) throws IOException {
        unZipFiles(new File(zipPath), descDir);
    }

    /**
     * 解压文件到指定目录 解压后的文件名，和之前一致
     *
     * @param zipFile 待解压的zip文件
     * @param descDir 指定目录
     */
    @SuppressWarnings("rawtypes")
    public void unZipFiles(File zipFile, String descDir) throws IOException {

        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        String textFiled="Successfully unzipped files:\n";
        String extractFailString="\nUnzip Errors:\n";
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
        ZipFile zip = new ZipFile(zipFile, Charset.forName("GBK"));//解决中文文件夹乱码  
        String name = zip.getName().substring(zip.getName().lastIndexOf('\\') + 1, zip.getName().lastIndexOf('.'));

//        File pathFile = new File(descDir + name);
        File pathFile = new File(descDir);
        if (!pathFile.exists()) {
            pathFile.mkdirs();
        }
        //unzip
        //System.out.println("1" + pathFile.getAbsolutePath() + "__________" + pathFile.getName());

        //File fileWithNewName = new File(recentPath);
        //System.out.println(zipFile.getName());
        String newN = zip.getName().substring(0, zip.getName().lastIndexOf("."));
        File newName = new File(descDir + newN);
        if (pathFile.isDirectory()) {
            pathFile.renameTo(newName);
            //System.out.println("2" + pathFile.getAbsolutePath() + "__________" + pathFile.getName());
        } else {
            pathFile.mkdir();
            pathFile.renameTo(newName);
            
           // System.out.println("3" + pathFile.getAbsolutePath() + "__________" + pathFile.getName());
        }

        for (Enumeration<? extends ZipEntry> entries = zip.entries(); entries.hasMoreElements();) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            String zipEntryName = entry.getName();
            InputStream in = zip.getInputStream(entry);

            //String outPath = (descDir + name +"/"+ zipEntryName).replaceAll("\\*", "/");  
            String outPath = (descDir + "/" + zipEntryName).replaceAll("\\*", "/");

            // 判断路径是否存在,不存在则创建文件路径  
            File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
            if (!file.exists()) {
                file.mkdirs();
                textFiled+="-" + file.getName() + "\n";
                //得到里面的东西
              //  System.out.println("4" + file.getAbsolutePath() + "__________" + file.getName());
            }

            // 判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压  
            if (new File(outPath).isDirectory()) {

                //System.out.println("5" + file.getAbsolutePath() + "__________" + file.getName());
                continue;
                
            }
//            textFiled+="-" + file.getName() + "\n";
            // 输出文件路径信息  
            //System.out.println("outpath is: "+"/"+outPath);  

            FileOutputStream out = new FileOutputStream(outPath);
            byte[] buf1 = new byte[1024];
            int len;
            while ((len = in.read(buf1)) > 0) {
                out.write(buf1, 0, len);
            }
            in.close();
            out.close();

        }
        
        workspace.unzipTextField.setEditable(false);
            workspace.unzipTextField.setText(textFiled + extractFailString);
        System.out.println("******************解压完毕********************");
        System.out.println();
        return;
    }

    void extraZip(String zipfile) throws ZipException, IOException {
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipfile));
        ZipEntry entry = zipIn.getNextEntry();
        File file = new File(zipfile);
        String textFiled = "Successfully extract files:\n";
        String extractFailString = "\nSubmission Errors:\n";
        
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
        // iterates over entries in the zip file
        while (entry != null) {
            File destDir = new File(FILE_PATH + app.getGUI().getAppTitle() + "/submissions/");
            if (!destDir.exists()) {
                destDir.mkdir();
            }

            String filePath = FILE_PATH + app.getGUI().getAppTitle() + "/submissions/" + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                //extractFile(zipIn, filePath);
                if (entry.getName().endsWith(".zip")) {


                    Slide slide = new Slide(entry.getName(), filePath + "/" + entry.getName(), "");
                    //这是第二个
                    data.getSlide1().add(slide);

                    //这是第  3 个
                    data.getSlide2().add(slide);

                    //这是第   个
                    data.getSlide3().add(slide);
                    //add successful to textfield
                    textFiled += "-" + entry.getName() + "\n";

                } else {

                    extractFailString += "-" + entry.getName() + "\n";
                    //System.out.println("Error!!!!!!!!---------------------" + entry.getName() + "file format");
                }

                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
                byte[] bytesIn = new byte[BUFFER_SIZE];
                int read = 0;
                while ((read = zipIn.read(bytesIn)) != -1) {
                    bos.write(bytesIn, 0, read);
                }
                bos.close();
            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();

        }
        zipIn.close();

        workspace.getNextButton().setDisable(false);
        workspace.getRenameButton().setDisable(false);
        workspace.getCaptionTextField().setEditable(false);
        workspace.getCaptionTextField().setText(textFiled + extractFailString);
    }

    void renameCurrentTable(TableView<Slide> slide) {

        String textFiled = "Successfully renamed sunmissions:\n";
        String extractFailString = "\nRename Errors:\n-none";
        SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        //  System.out.println(data.getSlide3().get(0).getFileName());
        int index = 0;
        int i = 0;
        for (Slide sli : slide.getItems()) {
            String path = sli.getPath();
            //System.out.println(path+"is file path");
            String[] pathName = path.split("/");

            String actualFileName = pathName[pathName.length - 1];
            String[] changeName = actualFileName.split("_");
            //System.out.println(sli.getFileName() + "------"+ changeName[1]);

            if (sli.getFileName().equalsIgnoreCase(data.getSlide3().get(index).getFileName())) {
                data.getSlide3().get(index).setFileName(changeName[1] + ".zip");
                File fileWithNewName = new File("./work/" + app.getGUI().getAppTitle() + "/submissions/" + actualFileName + "/");

                String newPath = "./work/" + app.getGUI().getAppTitle() + "/submissions/" + changeName[1] + ".zip/";
                File newName = new File(newPath);
                if (fileWithNewName.isFile()) {
                    textFiled += "-" + fileWithNewName.getName() + "\n";
                    fileWithNewName.renameTo(newName);

                } else {
                    //fileWithNewName.mkdir();
                    fileWithNewName.renameTo(newName);

                }
                index++;
                //     System.out.println("index------" + index);
            }
            workspace.getCaptionTextField().setEditable(false);
            workspace.renameTextField.setText(textFiled + extractFailString);
        }

        workspace.getNextButton().setDisable(false);
    }
    static int count = 0;

    void handleExtractCode(String str, String path, File codeFile) {
//        String textFiledfind = "Successfully code extraction :\n";
//        String extractFailString = "\nCode Extraction Errors:\n-none";

        Scanner scan = new Scanner(System.in);

        String exegc = "^.+" + str + "$";  //匹配以str为后缀名的文件；
        File file = new File(path);
        if (!file.exists()) {
            System.out.println("您要查找的路径不存在，请重新输入：");
//            findPath();
        } else if (!file.isDirectory()) {
            System.out.println("您输入的不是文件夹，请重新输入：");
//            findPath();
        } else {
            find(file, exegc, codeFile);   //调用方法；

            if (count == 0) {
                System.out.println("没有您要找的" + str + "为后缀名的文件");
            } else {
                System.out.println(path + "文件夹下共有以" + count + "个以" + str + "为后缀名的文件");
            }

//                    ZipInputStream zipIn = new ZipInputStream(new FileInputStream(codeFile));
//        ZipEntry entry = zipIn.getNextEntry();
            //textFiledfind += "-" + entry.getName() + "\n";
        }

//        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
//        workspace.codeTextField.setEditable(false);
//        workspace.codeTextField.setText(textFiledfind + extractFailString);
    }

    static int count2 = 0;

    void handleExtractCode(String str, String str2, String str3, String path, File codeFile) {
        String textFiledfind = "Successfully code extraction :\n";
        String extractFailString = "\nCode Extraction Errors:\n-none";

        Scanner scan = new Scanner(System.in);
        //System.out.println("请输入您要查找的文件夹的完整路径：");
        //String path = "/Users/macbook/Desktop/HW4/HW1Solution/work/留着/projects/fellin";
        //System.out.println("请输入您要查找的文件的后缀名：：");
        //String str = scan.nextLine();

        String exegc = "^.+" + str + "$";
        String exegc2 = "^.+" + str2 + "$";
        String exegc3 = "^.+" + str3 + "$";//匹配以str为后缀名的文件；
        File file = new File(path);
        if (!file.exists()) {
            System.out.println("您要查找的路径不存在，请重新输入：");
//            findPath();
        } else if (!file.isDirectory()) {
            System.out.println("您输入的不是文件夹，请重新输入：");
//            findPath();
        } else {
            find(file, exegc, codeFile);
            find(file, exegc2, codeFile);
            find(file, exegc3, codeFile);//调用方法；
            if (count == 0) {
                System.out.println("no ." + str + " file");
            } else {
                System.out.println(path + "there are" + count + " ." + str + "files");
            }
        }

        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
//        workspace.codeTextField.setEditable(false);
//        workspace.codeTextField.setText(textFiledfind + extractFailString);
    }

    private void find(File file1, String exegc, File codeFile) {
        String textFiledfind = "Successfully code extraction :";
        String extractFailString = "\n\nCode Extraction Errors:\n-none";

        File[] lists = file1.listFiles();  //将当前文件夹下的文件和文件夹放入数组中

        for (int i = 0; i < lists.length; i++) {
            if (lists[i].isDirectory()) {
                find(lists[i], exegc, codeFile);
            } else {
                if (lists[i].getName().matches(exegc)) {
                    System.out.println(lists[i].getAbsolutePath());
                    

                    //需要复制的目标文件或目标文件夹  
                    String pathname = lists[i].getAbsolutePath();
                    File file = new File(pathname);
                    //复制到的位置  
                    String topathname = codeFile.getAbsolutePath();
                    File toFile = new File(topathname);
                    
                    try {
                        copy(file, toFile);
                        
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block  
                        e1.printStackTrace();
                    }
                    count++;
                }
            }
            textFiledfind+="\n-"+lists[i].getName()+"\n---"+lists[i].getPath();
        }
        SlideshowCreatorWorkspace workspace = (SlideshowCreatorWorkspace) app.getWorkspaceComponent();
        workspace.codeTextField.setEditable(false);
        workspace.codeTextField.setText(textFiledfind + extractFailString);

    }

    public static void copy(File file, File toFile) throws Exception {
        byte[] b = new byte[1024];
        int a;
        FileInputStream fis;
        FileOutputStream fos;
        if (file.isDirectory()) {
            String filepath = file.getAbsolutePath();
            filepath = filepath.replaceAll("\\\\", "/");
            String toFilepath = toFile.getAbsolutePath();
            toFilepath = toFilepath.replaceAll("\\\\", "/");
            int lastIndexOf = filepath.lastIndexOf("/");
            toFilepath = toFilepath + filepath.substring(lastIndexOf, filepath.length());
            File copy = new File(toFilepath);
            //复制文件夹  
            if (!copy.exists()) {
                copy.mkdir();
            }
            //遍历文件夹  
            for (File f : file.listFiles()) {
                copy(f, copy);
            }
        } else {
            if (toFile.isDirectory()) {
                String filepath = file.getAbsolutePath();
                filepath = filepath.replaceAll("\\\\", "/");
                String toFilepath = toFile.getAbsolutePath();
                toFilepath = toFilepath.replaceAll("\\\\", "/");
                int lastIndexOf = filepath.lastIndexOf("/");
                toFilepath = toFilepath + filepath.substring(lastIndexOf, filepath.length());

                //写文件  
                File newFile = new File(toFilepath);
                fis = new FileInputStream(file);
                fos = new FileOutputStream(newFile);
                while ((a = fis.read(b)) != -1) {
                    fos.write(b, 0, a);
                }
            } else {
                //写文件  
                fis = new FileInputStream(file);
                fos = new FileOutputStream(toFile);
                while ((a = fis.read(b)) != -1) {
                    fos.write(b, 0, a);
                }
            }

        }
    }

//    public static void main(String[] args){
//        
//    }
//        public void recent(File file){
//            SlideshowCreatorData data = (SlideshowCreatorData) app.getDataComponent();
//
//            Slide slide = new Slide(file.getName()+".zip", file.getAbsolutePath(), "");
//                    data.getSlide1().add(slide);
//        }
}
